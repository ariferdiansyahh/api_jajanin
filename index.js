const express = require('express');
const app = express();
const serverless = require('serverless-http')
const bodyParser = require('body-parser');
app.use(bodyParser.json());

//DATABASE
const db = require('./src/config/env')
//DB TEST
db.authenticate()

//ROUTER
require('./src/router/route')(app);

const port = process.env.PORT || 5000;

app.listen(port, console.log(`server start on port ${port}`));