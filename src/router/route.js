const controller = require('../controller/controller')
const {check, validationResult} = require('express-validator')
module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.get('/', (req, res) => res.send('index'));
    app.post('/users/create',[
        check('email').isEmail(),
        check('password').isLength({min:5}),
        check('username').isLength({min:2}),
        check('shop_name').isLength({min:2}),
        check('no_phone').isNumeric()
        ], 
    controller.createUser);
    app.put('/users/edit/:id',[
        check('email').isEmail(),
        check('username').isLength({min:2}),
        check('shop_name').isLength({min:2}),
        check('no_phone').isNumeric()
        ], controller.editUser);
    app.post('/users/login', controller.loginUser);
    app.get('/users/:uid/product',controller.getProduct )
    app.get('/users/product/:id', controller.getProductById)
    app.post('/users/:uid/create/product',[
        check('product').isLength({min:2}),
        check('type_product').isLength({min:2}),
        check('price').isLength({min:3}),
        check('amount_product').isLength({min:1}),
        ],  controller.createProduct);
    app.put('/users/:id/edit/product' ,[
        check('product').isLength({min:2}),
        check('type_product').isLength({min:2}),
        check('price').isLength({min:3}),
        check('amount_product').isLength({min:1}),
        ], controller.editProduct);
    app.delete('/users/:id/delete/product', controller.deleteProduct);
    app.post('/users/:uid/create/transaction', [
        check('total_price').isLength({min:1}),
        check('item').isLength({min:2})
    ],controller.createTransaction)
    app.delete('/users/:id/delete/transaction', controller.deleteTransaction)
    app.get('/users/:uid/transaction', controller.getTransaction)
    app.get('/users/:uid/detail', controller.getDetail )
}