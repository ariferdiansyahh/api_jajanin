const Sequelize = require('sequelize');
const db = require('../config/env');
const users = db.define('products', {
    uid: {
        type: Sequelize.BIGINT
    },
    product: {
        type: Sequelize.STRING
    },
    type_product:{
        type: Sequelize.STRING
    },
    price:{
        type: Sequelize.FLOAT
    },
    amount_product:{
        type: Sequelize.BIGINT
    }
},{
    underscored:true
});

module.exports = users;