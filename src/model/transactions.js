const Sequelize = require('sequelize');
const db = require('../config/env');
const users = db.define('transactions', {
    uid: {
        type: Sequelize.BIGINT
    },
    total_price: {
        type: Sequelize.FLOAT
    },
    item:{
        type: Sequelize.TEXT
    }
},{
    underscored:true
});

module.exports = users;