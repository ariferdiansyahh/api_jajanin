const Sequelize = require('sequelize');
const db = require('../config/env');
const users = db.define('users', {
    username: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password:{
        type: Sequelize.STRING
    },
    shop_name:{
        type: Sequelize.STRING
    },
    no_phone:{
        type: Sequelize.STRING
    }
},{
    underscored:true
});

module.exports = users;