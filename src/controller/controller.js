const express = require('express')
const Sequelize = require('sequelize');
const config = require('../config/config')
const tb = require('../config/tb_config')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {check, validationResult} = require('express-validator')
// Load the full build.
var _ = require('lodash');
// Load the core build.
var _ = require('lodash/core');
// Load the FP build for immutable auto-curried iteratee-first data-last methods.
var fp = require('lodash/fp');
 
// Load method categories.
var array = require('lodash/array');
var object = require('lodash/fp/object');
 
// Cherry-pick methods for smaller browserify/rollup/webpack bundles.
var at = require('lodash/at');
var curryN = require('lodash/fp/curryN');
const users = tb.users;
const products = tb.products;
const transactions = tb.transactions;
exports.createUser = async (req, res) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        var numSaltRounds = 10;
        const data = await users.create({
            email: req.body.email,
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password,numSaltRounds),
            shop_name : req.body.shop_name,
            no_phone : req.body.no_phone      
        });
        return res.json(data)
    } catch (err) {
        console.log(err)
    }
}

exports.editUser = async (req,res) => {
    try{
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        var numSaltRounds = 10
        const data = await users.update({
            email: req.body.email,
            username: req.body.username,
            shop_name: req.body.shop_name,
            no_phone : req.body.no_phone   
        },
        {
            where : {
                id:req.params.id,
            }
        });

        return res.json({
            "status" : "updated",
            data:{
                id:req.params.id,
                email: req.body.email,
                username: req.body.username,
                shop_name: req.body.shop_name,
                no_phone : req.body.no_phone  
            }
        })
    }catch(err){
        console.log(err)
    }
}

exports.loginUser = async (req,res) => {
    try{
        const data = await users.findOne({
            where:{
                email : req.body.email
            }
        })
        if (!data) {
            return res.status(404).send('User Not Found.');
        }
        const passwordIsValid = bcrypt.compareSync(req.body.password, data.password);
        if (!passwordIsValid) {
            return res.status(401).send({
                auth: false,
                accessToken: null,
                reason: "Invalid Password!"
            });
        }

        const tokenCode = jwt.sign({
            email : data.email
        }, 'secret',{
            expiresIn: 86400
        })
        res.status(200).json({
            auth: true,
            accessToken: tokenCode,
            data: data
        });
    } catch(err){
        console.log(err)
    }
}

exports.getProduct = async (req,res) => {
    try{
        const data = await products.findAll({
            where: {
                uid: req.params.uid
            }
        })
        const makanan = _.filter(data, {
            'type_product' : 'makanan'
        })
        const minuman = _.filter(data, {
            'type_product' : 'minuman'
        })
        const barang_lain = _.filter(data, {
            'type_product' : 'barang_lain'
        })
        res.status(200).json({
            status : true,
            message: "Succes GetMenu",
            data : {
                makanan,
                minuman,
                barang_lain
            }
        });
    }catch(err){
        console.log(err)
    }
}
exports.getProductById = async (req,res) => {
    try{
        const data = await products.findOne({
            where:{
                id:req.params.id
            }
        })
        res.status(200).json({
            status:true,
            message:"Success GetMenuById",
            data
        })
    }catch(err){
        console.log(err)
    }
}
exports.createProduct = async (req,res) => {
    try{ 
        // const now = new Date();
        // const time = now.setHours(now.getHours()+4)
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        const data = await products.create({
            uid: req.params.uid,
            product : req.body.product,
            type_product : req.body.type_product,
            price : req.body.price,
            amount_product : req.body.amount_product
        })
        res.status(200).json({
            status: "Succes",
            data
        })
    }catch(err){
        console.log(err);
    }
}

exports.editProduct = async (req,res) => {
    try{
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        await products.update({
            product : req.body.product,
            type_product : req.body.type_product,
            price : req.body.price,
            amount_product : req.body.amount_product
        },{
            where : {
                id:req.params.id
            }
        })
        res.status(200).json({
            status: true,
            message : "Product Updated",
            data:{
                product : req.body.product,
                type_product : req.body.type_product,
                price : req.body.price,
                amount_product : req.body.amount_product
            }
        })
    }catch(err){
        console.log(err)
    }
}

exports.deleteProduct = async (req,res) => {
    try{
        const data = await products.destroy({
            where: {
                id: req.params.id
            }
        })

        res.status(200).json({
            status: true,
            message : "Deleted Product"
        })
    }catch(err){
        console.log(err)
    }
}

exports.createTransaction = async (req, res) => {
    try{
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        const data = await transactions.create({
            uid: req.params.uid,
            total_price: req.body.total_price,
            item: JSON.stringify(req.body.item)
        })
        
        res.status(200).json({
            status: true,
            message: "Create Transactions",
            data
        })
    }catch(err){
        console.log(err)
    }
}

exports.deleteTransaction = async (req,res) => {
    try{
        const data = await transactions.destroy({
            where:{
                id:req.params.id
            }
        })

        res.status(200).json({
            status: true,
            message: "Delete Transactions",
        })
    }catch(err){
        console.log(err);
    }
}

exports.getTransaction = async (req,res) => {
    try{
        const data = await transactions.findAll({
            where:{
                uid:req.params.uid
            }
        })
        res.status(200).json({
            status: true,
            message:"Get Transactions",
            data
        })
    }catch(err){
        console.log(err)
    }
}

exports.getDetail = async(req,res) => {
    try{
        const dataPendapatan = await transactions.findAll({
            where:{
                uid:req.params.uid
            }
        })
        var pendapatan = 0
        for(var i=0; i<dataPendapatan.length; i++){
            pendapatan += parseInt(dataPendapatan[i].total_price, 10);
        }
        const totalTransaksi = dataPendapatan.length

        const barang = await products.findAll({
            where: {
                uid: req.params.uid
            }
        })
        const totalBarang = barang.length
        res.status(200).json({
            status:true,
            data:{
                pendapatan:pendapatan,
                total_transaksi:totalTransaksi,
                totalBarang:totalBarang
            }
        })
    }catch(err){
        console.log(err)
    }
}