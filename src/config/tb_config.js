
const Sequelize = require('sequelize')
const db = {}
db.Sequelize = Sequelize;
db.users = require('../model/users')
db.products = require('../model/products')
db.transactions = require('../model/transactions')
// db.users.hasMany(db.products, {
//     foreignKey: 'uid',
//     otherKey: 'id'
// });
module.exports = db;